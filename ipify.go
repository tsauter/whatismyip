package main

import (
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

const (
	API_URI    = "https://api.ipify.org"
	USER_AGENT = "whatismyip"
)

type IPIFY struct {
}

func (i *IPIFY) GetIP() (string, error) {
	netTransport := &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	client := &http.Client{
		Timeout:   time.Second * 10,
		Transport: netTransport,
	}

	req, err := http.NewRequest("GET", API_URI, nil)
	if err != nil {
		return "", errors.Wrap(err, "failed to create request")
	}

	req.Header.Add("User-Agent", USER_AGENT)

	resp, err := client.Do(req)
	defer resp.Body.Close()

	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.Wrap(err, "failed to read data")
	}
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("Server return error: %d", resp.StatusCode)
	}

	return strings.TrimSpace(string(ip)), nil
}
