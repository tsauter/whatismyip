package main

import (
	"fmt"
	"github.com/fatih/color"
)

func main() {
	ipify := IPIFY{}
	ip, err := ipify.GetIP()
	if err != nil {
		fmt.Printf("Couldn't get my IP address: %v\n", err)
		return
	}

	fmt.Printf("%s: %s\n", color.GreenString("IP"), ip)
}
